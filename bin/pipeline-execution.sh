#!/bin/bash
# Use this script at your own risk. 

##########################################################
## Variables needed
##########################################################
# Please, change the following variables to fit your needs
# Group ID
GROUP_ID=net.sxgio.test
# Artifact Name
ARTIFACT_ID=java-bundle-poc
# Final Version
CURRENT_VERSION=0.1.0-SNAPSHOT
# Kind of Packaging
PACKAGING=jar
# KeyStore to push content to the nexus server.
# Leave it blank just in case you want the keystore automatically generated on the fly.  
KEYSTORE=keyStore/nexusmaster.keystore
# Nexus certification just in case you want to make the keystore on the fly. 
KEYSTORE_CERTIFICATE=keyStore/nexusmaster-alm.cer
# Keystore verification
KEYSTORE_VERIFICATION_PASSED=1
# Keystore alias
KEYSTORE_ALIAS="sanes-nexus"
# Keystore password
KEYSTORE_PASSWORD="test0123?"
# Name of the war file.
FILE="java-bundle-poc.${PACKAGING}"
# File content
FILES=`ls files`
# Alternative URL
NEXUS_SERVER=[your-server]
# ALT_URL=[Place here your URL just in case it's different from NEXUS_SERVER variable]
ALT_URL=${NEXUS_SERVER}
# Repository URL
REP_URL=${NEXUS_SERVER}/[your-repository]
# Settings Identifier to validate against the Repository previously choosen. 
REPOSITORY_ID=[your-repository-ID]

##########################################################
## Script Execution
##########################################################
# Jar execution
if [ ! -f $FILE ] ; then
    echo "Packaging ${PACKAGING} file named as ${FILE}"
    cd files && $JAVA_HOME/bin/jar -cvf ../${FILE} ${FILES} && cd ..
    # it does not exist yet.
    if [ ! -f $FILE ]; then
        echo "Error found. Neither the files to create the war file were found or the latter does not exist itself"
        exit 1 
    fi
fi

# keyStore verification
if [ -f ${KEYSTORE} ]; then
    echo "Verificating keystore ${FILE}"
    keytool -list -noprompt \
     -keystore ${KEYSTORE} \
     -alias ${KEYSTORE_ALIAS} \
     -storepass ${KEYSTORE_PASSWORD}

    if [ $? -eq 0 ]; then
        echo "Keystore ${KEYSTORE} is valid"
        KEYSTORE_VERIFICATION_PASSED=0
    else
        echo "Error found: Keystore ${KEYSTORE} is not valid. Trying to generate it on the fly"
    fi 
fi

# Keystore generation
if [ ${KEYSTORE_VERIFICATION_PASSED} -ne 0 ] && [ -f ${KEYSTORE_CERTIFICATE} ]; then
    echo "Executing keytool to make keystore on the fly"
    keytool -import -noprompt \
        -alias ${KEYSTORE_ALIAS} \
        -file ${KEYSTORE_CERTIFICATE} \
        -keystore ${KEYSTORE} \
        -storepass ${KEYSTORE_PASSWORD}\
        -keypass ${KEYSTORE_PASSWORD}
    if [ $? -ne 0 ]; then
        echo "Error found. Neither the keystore file was found or the keystore verification was not correct"
        exit 1
    fi 
fi

# Deploy execution 
echo "Executing maven deploy:deploy-file"
echo "mvn deploy:deploy-file -DgroupId=${GROUP_ID} -DartifactId=${ARTIFACT_ID} -Dversion=${CURRENT_VERSION} -Dpackaging=${PACKAGING} -DaltDeploymentRepository==${REPOSITORY_ID}::default::${ALT_URL} -Dfile=${FILE} -Durl=${REP_URL} -DrepositoryId=${REPOSITORY_ID} -Djavax.ssl.trustStore=${KEYSTORE}"
mvn deploy:deploy-file -DgroupId=${GROUP_ID} \
 -DartifactId=${ARTIFACT_ID} -Dversion=${CURRENT_VERSION} \
 -Dpackaging=${PACKAGING} -DaltDeploymentRepository=${REPOSITORY_ID}::default::${REP_URL} \
 -Dfile=${FILE} -Durl=${REP_URL} -DrepositoryId=${REPOSITORY_ID} \
 -Djavax.ssl.trustStore=${KEYSTORE}

# What happened?
retval=$?
echo "Maven execution returned ${retval}"
# End of pushing content to the nexus server
exit $retval

