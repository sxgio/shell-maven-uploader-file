# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Released]

## 0.1.0

## 2019-11-19

## Changed

- Markdown files modified.
- Last changes to pipeline-execution.sh
- Tested in alm nexusmaster.

## [Unreleased]

## 2019-11-18

## Changed 

- Markdown files modified.

## 2019-10-11

## Added

- Shell Bundle jar example
