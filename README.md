# Shell Script to push a packaging file in a Nexus Server

Sample script to push any war, jar, ear file in a nexus server. Some further configuration must be applied.

## Introduction

This project contains a script to be used as a guideline for some pipelines needed of pushing a pre-created war, jar or ear file to a nexus server. The script has not been customized to be fed by command line. However, it's needed to get into it and change those variables to fit your needs. Said so, use it at your own risk.

[![Git Repository](https://img.shields.io/badge/repository-url-green?style=flat&logo=git)](https://bitbucket.org/sxgio/shell-maven-uploader-file.git)

## Execution

Please, use the following command executed from the repository root.

```{.sh}
bin/pipeline-execution.sh
```

## Where to see results

If everything went right, please, check the nexus server of your choice and see if the jar, war, ear file was pushed to the nexus server.  

## Contributors

**Sergio Martín** <sergio.martin@sxgio.net>

## Contributing

[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://bitbucket.org/sxgio/shell-maven-uploader-file/src/master/CONTRIBUTING.md)

## License

© 2019. SXGIO.NET
